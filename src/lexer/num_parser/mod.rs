mod floats;
mod integers;

use super::Warning;
use std::fmt;

#[derive(Clone, Debug, Copy, PartialEq)]
pub(super) enum IntOrFloat {
    Int(i64),
    Float(f64),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum NumberError {
    /// No digits were found in the number
    Empty,
    Float(lexical_parse_float::Error),
    Int(lexical_parse_float::Error),
}

impl fmt::Display for NumberError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Empty => f.write_str("empty number"),
            Self::Float(err) => fmt::Display::fmt(err, f),
            Self::Int(err) => fmt::Display::fmt(err, f),
        }
    }
}

pub(super) fn parse_number(number: &str) -> Result<(IntOrFloat, Option<Warning>), NumberError> {
    if detect_float(number) {
        match self::floats::parse_float(number) {
            // TODO: post-processing warnings ?
            Ok(float) => Ok((IntOrFloat::Float(float), None)),
            Err(err) => Err(NumberError::Float(err)),
        }
    } else {
        match self::integers::parse_int(number) {
            // TODO: post-processing warnings ?
            Ok(int) => Ok((IntOrFloat::Int(int), None)),
            Err(err) => Err(NumberError::Int(err)),
        }
    }
}

/// Returns `true` is `number` should be parsed as a float
fn detect_float(number: &str) -> bool {
    if number.contains('.') {
        return true;
    }
    if !number.starts_with("0b")
        && !number.starts_with("0B")
        && !number.starts_with("0o")
        && !number.starts_with("0O")
        && !number.starts_with("0x")
        && !number.starts_with("0X")
    {
        number.contains('e') || number.contains('E')
    } else {
        false
    }
}
