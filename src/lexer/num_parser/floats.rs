use lexical_parse_float::{Error, FromLexicalWithOptions as _, NumberFormatBuilder};
use std::num::NonZeroU8;

/// Common format options for the various float formats.
const FORMAT_BASE: NumberFormatBuilder = NumberFormatBuilder::new()
    .required_integer_digits(true)
    .digit_separator(NonZeroU8::new(b'_'))
    .internal_digit_separator(true)
    .trailing_digit_separator(true)
    .consecutive_digit_separator(true);

/// Common format options for the various non-decimal float formats.
const NON_DECIMAL_BASE: NumberFormatBuilder = FORMAT_BASE
    .no_exponent_notation(true)
    .no_float_leading_zeros(false)
    .no_special(true);

/// This describe the format of a float that does not starts with `"0b"`, `"0o"`
/// or `"0x"`.
///
/// Such a float:
/// - Have digits in the range `'0'..='9'`.
/// - Can have a integral, fractional and exponent part.
///   - The integral part is at the beginning.
///   - If there is a `'.'`, digits after it (until an eventual `'e'`) are part
/// of the fractional part.
///   - If there is a `'e'`, digits after it are part of the exponent part. The
/// exponent can also have a leading `'-'`.
///
///   It must have a non-empty integral part, and either a fractional or exponent
/// part, or both.
///   ### Example
///   - 123 **is not** a valid float.
///   - 1.2 is a valid float.
///   - 1.2e4 is a valid float.
///   - 1e-4 is a valid float.
///   - 1e4.2 **is not** a valid float.
/// - All components can have `_` separators everywhere, except at the beginning.
/// - `"NaN"` is _not a number_.
/// - `"Inf"` is _infinity_.
const DECIMAL_FORMAT: u128 = FORMAT_BASE
    .radix(10)
    .no_exponent_notation(false)
    .exponent_base(NonZeroU8::new(10))
    .exponent_radix(NonZeroU8::new(10))
    .case_sensitive_special(true)
    .no_float_leading_zeros(true)
    .no_special(false)
    .build();

const BINARY_FORMAT: u128 = NON_DECIMAL_BASE
    .radix(2)
    .base_prefix(NonZeroU8::new(b'b'))
    .build();

const OCTAL_FORMAT: u128 = NON_DECIMAL_BASE
    .radix(8)
    .base_prefix(NonZeroU8::new(b'o'))
    .build();

const HEXADECIMAL_FORMAT: u128 = NON_DECIMAL_BASE
    .radix(16)
    .base_prefix(NonZeroU8::new(b'x'))
    .build();

#[allow(clippy::panic)] // panics are OK in `const`
const DECIMAL_OPTIONS: lexical_parse_float::Options =
    match lexical_parse_float::OptionsBuilder::new()
        .decimal_point(b'.')
        .exponent(b'e')
        .nan_string(Some(b"NaN"))
        .inf_string(Some(b"Inf"))
        .build()
    {
        Ok(options) => {
            if !options.is_valid() {
                panic!("Error while building decimal float parsing options")
            }
            options
        }
        Err(_) => panic!("Error while building decimal float parsing options"),
    };

#[allow(clippy::panic)] // panics are OK in `const`
const NON_DECIMAL_OPTIONS: lexical_parse_float::Options =
    match lexical_parse_float::OptionsBuilder::new()
        .decimal_point(b'.')
        .exponent(b'\n') // there are no exponents.
        .nan_string(None)
        .inf_string(None)
        .build()
    {
        Ok(options) => {
            if !options.is_valid() {
                panic!("Error while building non decimal float parsing options")
            }
            options
        }
        Err(_) => panic!("Error while building non decimal float parsing options"),
    };

pub(super) fn parse_float(input: &str) -> Result<f64, Error> {
    if input.starts_with("0b") || input.starts_with("0B") {
        f64::from_lexical_with_options::<{ BINARY_FORMAT }>(input.as_bytes(), &NON_DECIMAL_OPTIONS)
    } else if input.starts_with("0o") || input.starts_with("0O") {
        f64::from_lexical_with_options::<{ OCTAL_FORMAT }>(input.as_bytes(), &NON_DECIMAL_OPTIONS)
    } else if input.starts_with("0x") || input.starts_with("0X") {
        f64::from_lexical_with_options::<{ HEXADECIMAL_FORMAT }>(
            input.as_bytes(),
            &NON_DECIMAL_OPTIONS,
        )
    } else {
        f64::from_lexical_with_options::<{ DECIMAL_FORMAT }>(input.as_bytes(), &DECIMAL_OPTIONS)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    // TODO: find a way to fail parsing on strings like `"0b10"` (which should be an integer).
    // This is not a big deal right now, because we will never try to parse such strings anyways.
    // TODO: test extreme floats (clamp to inf, excessive precision...)
    #[allow(clippy::float_cmp)]
    fn test_float_parse() {
        // binary format
        assert_eq!(parse_float("0b10.1").unwrap(), 2.5f64);
        assert_eq!(parse_float("0B10.1").unwrap(), 2.5f64);
        assert_eq!(parse_float("0b0010_0100.1001_1000").unwrap(), 36.59375f64);
        // assert!(parse_float("0b10").is_err());
        assert!(parse_float("0bInf").is_err());
        assert!(parse_float("0bNan").is_err());
        // octal format
        assert_eq!(parse_float("0o35.7").unwrap(), 29.875f64);
        assert_eq!(parse_float("0O35.7").unwrap(), 29.875f64);
        assert_eq!(parse_float("0o0035.7").unwrap(), 29.875f64);
        // assert!(parse_float("0o35").is_err());
        assert!(parse_float("0oInf").is_err());
        assert!(parse_float("0oNan").is_err());
        // hexadecimal format
        assert_eq!(parse_float("0x5a2.f2").unwrap(), 1442.9453125f64);
        assert_eq!(parse_float("0X5A2.F2").unwrap(), 1442.9453125f64);
        assert_eq!(parse_float("0X5a2.f2").unwrap(), 1442.9453125f64);
        assert_eq!(parse_float("0x5A2.f2").unwrap(), 1442.9453125f64);
        assert_eq!(
            parse_float("0x000de_ad00.beef").unwrap(),
            14_593_280.745_834_35f64
        );
        // assert!(parse_float("0x5a2").is_err());
        assert!(parse_float("0xInf").is_err());
        assert!(parse_float("0xNan").is_err());
        // decimal format
        assert_eq!(parse_float("0.125").unwrap(), 0.125f64);
        assert!(parse_float("00.1").is_err()); // leading 0
        assert_eq!(parse_float("0.1_2__5_").unwrap(), 0.125f64);
        assert_eq!(parse_float("1e-2").unwrap(), 1e-2f64);
        assert_eq!(parse_float("1E-2").unwrap(), 1e-2f64);
        assert_eq!(parse_float("1.2e-2").unwrap(), 1.2e-2f64);
        assert!(parse_float("1e-2.2").is_err()); // '.' after 'E'
        assert!(parse_float("NaN").unwrap().is_nan());
        assert!(parse_float("-NaN").unwrap().is_nan());
        assert!(parse_float("Inf").unwrap().is_infinite());
        assert!(parse_float("-Inf").unwrap().is_infinite());
        assert!(parse_float("Inf__").is_err());
    }
}
