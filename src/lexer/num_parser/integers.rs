use lexical_parse_integer::{Error, FromLexicalWithOptions as _, NumberFormatBuilder};
use std::num::NonZeroU8;

/// Common format options for the various integer formats.
const FORMAT_BASE: NumberFormatBuilder = NumberFormatBuilder::new()
    .required_integer_digits(true)
    .digit_separator(NonZeroU8::new(b'_'))
    .internal_digit_separator(true)
    .trailing_digit_separator(true)
    .consecutive_digit_separator(true);

/// Common format options for the various non-decimal float formats.
const NON_DECIMAL_BASE: NumberFormatBuilder = FORMAT_BASE.no_integer_leading_zeros(false);

const DECIMAL_FORMAT: u128 = FORMAT_BASE.no_integer_leading_zeros(true).build();

const BINARY_FORMAT: u128 = NON_DECIMAL_BASE
    .radix(2)
    .base_prefix(NonZeroU8::new(b'b'))
    .build();

const OCTAL_FORMAT: u128 = NON_DECIMAL_BASE
    .radix(8)
    .base_prefix(NonZeroU8::new(b'o'))
    .build();

const HEXADECIMAL_FORMAT: u128 = NON_DECIMAL_BASE
    .radix(16)
    .base_prefix(NonZeroU8::new(b'x'))
    .build();

#[allow(clippy::panic)] // panics are OK in `const`
const INT_OPTIONS: lexical_parse_integer::Options =
    // Note: there are no actual options to set at the moment
    match lexical_parse_integer::OptionsBuilder::new().build() {
            Ok(options) => {
                if !options.is_valid() {
                    panic!("Error while building integer parsing options")
                }
                options
            }
            Err(_) => panic!("Error while building integer parsing options"),
        };

pub(super) fn parse_int(input: &str) -> Result<i64, Error> {
    if input.starts_with("0b") || input.starts_with("0B") {
        i64::from_lexical_with_options::<{ BINARY_FORMAT }>(input.as_bytes(), &INT_OPTIONS)
    } else if input.starts_with("0o") || input.starts_with("0O") {
        i64::from_lexical_with_options::<{ OCTAL_FORMAT }>(input.as_bytes(), &INT_OPTIONS)
    } else if input.starts_with("0x") || input.starts_with("0X") {
        i64::from_lexical_with_options::<{ HEXADECIMAL_FORMAT }>(input.as_bytes(), &INT_OPTIONS)
    } else {
        i64::from_lexical_with_options::<{ DECIMAL_FORMAT }>(input.as_bytes(), &INT_OPTIONS)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_integer_parse() {
        assert_eq!(parse_int("0b10").unwrap(), 0b10);
        assert_eq!(parse_int("0o35").unwrap(), 0o35);
        assert_eq!(parse_int("0x5a2").unwrap(), 0x5a2);
        assert_eq!(parse_int("0xDEAD_BEEF").unwrap(), 0xdead_beef);
        assert_eq!(parse_int("0xdead_beef").unwrap(), 0xdead_beef);
        assert_eq!(parse_int("0xDEAD_beef").unwrap(), 0xdead_beef);
        assert_eq!(parse_int("123456789").unwrap(), 123456789);
        assert_eq!(parse_int("-123456789").unwrap(), -123456789);
        assert_eq!(parse_int("1__234_56789___").unwrap(), 123456789);
    }
}
