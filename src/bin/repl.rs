use nox::{
    libraries,
    runtime::{Value, VirtualMachine, VmError},
    Continue,
};
use rustyline::{error::ReadlineError, history::MemHistory, Editor};
use std::io::{self, Write};

struct Repl {
    current_phrase: String,
    vm: VirtualMachine,
    editor: Editor<(), MemHistory>,
}

impl Repl {
    fn new() -> Result<Self, ReadlineError> {
        let mut vm = VirtualMachine::new();
        vm.import_all(libraries::std()).unwrap();
        Ok(Self {
            current_phrase: String::new(),
            vm,
            editor: Editor::with_history(rustyline::Config::default(), MemHistory::new())?,
        })
    }

    fn evaluate(&mut self) -> Result<(), VmError> {
        let warnings = self.vm.parse_top_level(&self.current_phrase)?;
        for warning in warnings {
            println!("{}", warning)
        }
        let value = self.vm.run()?;
        if value != Value::Nil {
            println!("=> {}", value);
        }
        Ok(())
    }

    fn prompt(&mut self) -> Result<(), ReadlineError> {
        io::stdout().flush().unwrap();
        let prompt = if self.current_phrase.is_empty() {
            "> "
        } else {
            "    "
        };
        let input = self.editor.readline(prompt)?;
        self.current_phrase.push_str(&input); // skip \n
        match self.evaluate() {
            Ok(_) => {
                self.editor
                    .add_history_entry(std::mem::take(&mut self.current_phrase))?;
            }
            Err(err) => match err.continuable() {
                Continue::Stop => {
                    println!("{}", err);
                    self.editor
                        .add_history_entry(std::mem::take(&mut self.current_phrase))?;
                }
                Continue::Continue => self.current_phrase.push('\n'),
            },
        }
        Ok(())
    }
}

fn real_main() -> Result<(), ReadlineError> {
    let mut top_level = Repl::new()?;
    loop {
        top_level.prompt()?;
    }
}

fn main() {
    if let Err(err) = real_main() {
        match err {
            ReadlineError::Eof | ReadlineError::Interrupted => {}
            _ => panic!("{err}"),
        }
    }
}
