use std::{
    ffi::OsStr,
    fmt::{Debug, Display},
    path::PathBuf,
};

use nox::runtime::{Value, VirtualMachine};

struct Error {
    error: Box<dyn Display>,
}

impl Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.error, f)
    }
}

impl<E: Display + 'static> From<E> for Error {
    fn from(err: E) -> Self {
        Self {
            error: Box::new(err),
        }
    }
}

fn main() -> Result<(), Error> {
    let (manifest_dir, manifest_dir_path) = match std::env::var_os("CARGO_MANIFEST_DIR") {
        Some(dir) => {
            let dir_path = PathBuf::from(dir).join("examples");
            match dir_path.read_dir() {
                Ok(dir) => (dir, dir_path),
                Err(err) => {
                    return Err(format!("error reading {}: {}", dir_path.display(), err).into())
                }
            }
        }
        None => return Err("variable 'CARGO_MANIFEST_DIR' is not set".into()),
    };
    for entry in manifest_dir {
        let entry = match entry {
            Ok(entry) => entry.path(),
            Err(error) => {
                return Err(
                    format!("error reading {}: {}", manifest_dir_path.display(), error).into(),
                )
            }
        };
        let extension = entry.extension();
        if extension == Some(OsStr::new("nox")) {
            println!("reading file {}:", entry.display());
        } else {
            continue;
        }
        let file_content = match std::fs::read_to_string(&entry) {
            Ok(content) => content,
            Err(error) => {
                return Err(format!("error reading {}: {}", entry.display(), error).into())
            }
        };

        let mut vm = VirtualMachine::new();
        let warnings = vm.parse_top_level(&file_content)?;
        vm.import_all(nox::libraries::std()).unwrap();
        for warning in warnings {
            println!("{}", warning)
        }
        match vm.run() {
            Ok(value) => {
                if value != Value::Nil {
                    println!("=> {}", value)
                }
            }
            Err(error) => return Err(format!("{}", error).into()),
        }
    }
    Ok(())
}

#[test]
fn test() -> Result<(), Error> {
    main()
}
